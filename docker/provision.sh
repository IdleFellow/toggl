#!/usr/bin/env sh

# This script sets up the environment and runs Terraform commands to deploy infrastructure on GCP.

# This script sets up the environment for running Terraform commands in a Docker container. 
# It first checks if a GCP key file (gcp-key.json) is present in the container, and sets the GOOGLE_APPLICATION_CREDENTIALS environment variable to point to it.
# Next, the script checks if several environment variables (TF_VAR_project, TF_VAR_db_password) have been set. 
# These variables are used by Terraform to specify the GCP project and database password, respectively. 
# If any of these variables are missing, the script will exit with an error message.
# The script then changes the current working directory to /terraform and initializes Terraform with terraform init. 
# If the NOACT environment variable is not set, the script will proceed to apply the Terraform plan. 
# If the AUTO_APPROVE environment variable is set, the script will automatically approve any changes without prompting the user.
# Otherwise, the script will prompt the user to confirm the changes before applying them. 
# If the NOACT environment variable is set, the script will only perform a Terraform plan and not actually apply any changes.

if [ -f /gcp-key.json ]; then
    export GOOGLE_APPLICATION_CREDENTIALS=/gcp-key.json
fi

if [ -z "$GOOGLE_APPLICATION_CREDENTIALS" ]; then
  echo "GCP credentials not set" 2>&1
  exit 1
fi

if [ -z "$TF_VAR_project" ]; then
  echo "TF_VAR_project not set" 2>&1
  exit 1
fi

if [ -z "$TF_VAR_db_password" ]; then
  echo "TF_VAR_db_password not set" 2>&1
  exit 1
fi

cd /terraform
terraform init
if [ -z "$NOACT" ]; then
    if [ -n "$AUTO_APPROVE" ]; then
        terraform apply -auto-approve
    else
        terraform apply
    fi
else
    terraform plan
fi