# Toggl

## TL;DR

If you are using Linux or MacOS:

1. Create a credential JSON file or take note of the JSON service account key used to authenticate to GCP

2. Make sure that you have the following prerequisites installed on your machine:
- A docker-compatible container manager
- The gcloud CLI executable

3. Create the resources with the script provision.sh
```
./provision.sh -p <gcp_project_id> -d <database_admin_user_password> -c [<gcp_key_file>|<gcp_key_json>]`
```
The '-c' option accepts either the JSON file path or the service account key in JSON notation.

4. Check the configuration changes and apply them by typing 'yes' at the prompt

5. Run the database initialisation job with the command displayed at the end of the script

If you are using another OS, or you want to know more about the resource provisioning process, see the Usage section for further details

## General info

This repository includes the files needed to complete the DBRE Home Assignment as defined **[here](https://toggl.notion.site/DBRE-Home-Assignment-99aadd44cbdd4a96b4103eef239ca437)**

This setup uses Terraform to provision the following deliverables:

### Primary Database

PostgreSQL instance running on 12, that will serve as primary(master) server. 

### Initialisation Job

Cloud Run Job to initialise the Primary Database with **[pgbench](https://www.postgresql.org/docs/current/pgbench.html)**.

### Standby Database

PostgreSQL instance that replicates primary database. 
The Standby Database is backed up every day using a cronjob. The backups are stored on a Cloud Storage bucket. 

### Cloud Storage

Cloud Storage bucket that will contain daily backups. Has a retention period of 15 days (after that backups are automatically deleted).

### Monitoring

The following alerts on Google Cloud Monitoring.

- When CPU Usage > 90% on Primary Database.
- When Disk Usage > 85% on Primary Database.

The alerts are configured to send an email message

## Implementation details

All the resources are provisioned with Terraform.

The default value for the "protected" property of the database instances is "true". Because this is just an exercise implementation, the instances are created with protected=false.

The initialisation job is a one-off task that must be executed once at database creation.
Being the process an imperative task, it is not handled by Terraform. The configuration files create a Cloud Run Job that must be triggered using the CLI.
The Terraform setup outputs the command to execute for the purpose.

## Usage

There are three ways to provision resources:
- Using a local version of Terraform 
- Using the docker container included in the 'docker' directory
- Using the script 'provision.sh'

### Using a local version of Terraform 

### Prerequisites

The Terraform executable. This setup has been tested with Terraform v1.4.0 on an M1 Mac.
The gcloud CLI executable
A JSON representation of your authorisation key for the Google Cloud Platform

#### How to create the resources

These are the commands needed to create the cloud resources:

Define the required environment variables:
```
export GOOGLE_APPLICATION_CREDENTIALS=[<gcp_key_file>|<gcp_key_json>]
export TF_VAR_project=<my project id>
export TF_VAR_db_password=<database password>
```


Edit the variables.tf file to adapt the default values to your configuration (Optional)

Execute Terraform:
```
cd terraform
terraform init
terraform plan
terraform apply
```


Run the database initialisation job:
```
gcloud beta run jobs execute <pgbench job name> --wait --region <your_gcp_region>`
```

### Using the docker container included in the 'docker' directory

### Prerequisites

A docker-compatible container manager
The gcloud CLI executable
A JSON representation of your authorisation key for the Google Cloud Platform

#### How to create the resources

These are the commands needed to create the cloud resources:

Create the docker image with:
```
docker build -t toggl docker`
```

If you saved the GCP credentials into a file, run the docker image with:
```
docker run \
  -it --rm \
  --name toggl \
  -v "$(pwd)/terraform":/terraform \
  -v <the_absolute_path_to_your_gcp_key_file>:/gcp-key.json \
  -e GOOGLE_APPLICATION_CREDENTIALS=/gcp-key.json \
  -e TF_VAR_project=<your_gcp_project_id> \
  -e TF_VAR_db_password=<database_admin_user_password> \
  toggl
```


If you have the JSON notation of your GCP credentials, run the docker image with:
```
docker run \
  -it --rm \
  --name toggl \
  -v "$(pwd)/terraform":/terraform \
  -e GOOGLE_APPLICATION_CREDENTIALS="<your_gcp_key_in_json_notation>" \
  -e TF_VAR_project=<your_gcp_project_id> \
  -e TF_VAR_db_password=<database_admin_user_password> \
  toggl
```


Run the database initialisation job:
```
gcloud beta run jobs execute <pgbench job name> --wait --region <your_gcp_region>
```

### Using the script 'provision.sh'

### Prerequisites

A docker-compatible container manager
The gcloud CLI executable
A JSON representation of your authorisation key for the Google Cloud Platform

#### How to create the resources

Launch the script with:
```
./provision.sh -p <gcp_project_id> -d <database_admin_user_password> -c [<gcp_key_file>|<gcp_key_json>]
```

The '-c' option accepts either the JSON file path or the service account key in JSON notation.

Other options are:
```
 -n ) do not apply the configuration: runs 'terraform plan' only
 -a ) auto approve configuration changes: runs 'terraform apply' with the '-auto-approve' option
```

Run the database initialisation job:
```
gcloud beta run jobs execute <pgbench job name> --wait --region <your_gcp_region>
```

## Possible Improvements

Better handling of secrets

Include gcloud in the docker image to run the initialisation script from there
