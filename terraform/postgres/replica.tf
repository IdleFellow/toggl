/*
 * This Terraform file is used to deploy a Cloud Run job that runs a Postgres benchmarking tool against a Cloud SQL instance.
 *
 * This Terraform file defines a Google Cloud Run job resource named "pgbench". 
 * The job will use a custom Docker image specified in the "pgbench_image" variable to run a Postgres benchmarking tool called "pgbench" 
 * against a Cloud SQL instance.
 * 
 * The job is defined in the "BETA" launch stage and located in the region specified in the "region" variable. 
 * The job is using a Cloud Run template to define its properties, including the job name, location, and Docker container configuration.
 * 
 * The Cloud SQL instance is mounted as a volume in the job using the "cloudsql" volume name, and its connection details are specified by the "connection_name" 
 * attribute of the "google_sql_database_instance.primary_instance" resource. 
 * The Postgres database name, username, and password used in the benchmarking command are specified by the "db_name", "db_user", and "db_password" variables, 
 * respectively.
 * 
 */
 
resource "google_sql_database_instance" "read_replica" {
  name                 = var.replica_name
  master_instance_name = var.instance_name
  region               = var.region
  database_version     = var.db_version

  deletion_protection = var.protected

  replica_configuration {
    failover_target = false
  }

  settings {
    tier              = var.db_tier
    availability_type = "ZONAL"
    disk_size         = var.db_size

    location_preference {
      zone = var.replica_zone
    }

    database_flags {
      name  = "cloudsql.iam_authentication"
      value = "on"
    }

    backup_configuration {
      enabled = false
    }

    ip_configuration {
      ipv4_enabled    = true
      private_network = var.private_network
    }
  }
}
