/*
 * This Terraform file is defining a Google Cloud SQL database instance using the google_sql_database_instance resource.
 * 
 * The instance is defined with a set of settings which includes specifying the name of the instance, the database version, and the tier. 
 * The deletion_protection parameter is also defined, which ensures that the instance cannot be deleted accidentally.
 * 
 * The availability_type is set to "ZONAL", which means that the instance is located in a single zone rather than multiple zones. 
 * The disk_size parameter defines the size of the disk in gigabytes.
 * 
 * The ip_configuration block specifies the IP configuration for the instance. 
 * In this case, IPv4 is enabled and a private network is specified using the private_network variable.
 * 
 * The database_flags block specifies a custom flag for the instance, which enables IAM authentication.
 * 
 * The backup_configuration block is set to false, which means that backups are not enabled.
 * 
 * Finally, the user_labels block is used to attach metadata to the instance, in this case specifying the name of the instance using the instance_name variable.
 */

resource "google_sql_database_instance" "primary_instance" {
  name                = var.instance_name
  database_version    = var.db_version

  deletion_protection = var.protected

  settings {
    tier = var.db_tier


    availability_type = "ZONAL"
    disk_size         = var.db_size

    location_preference {
      zone = var.zone
    }

    ip_configuration {
      ipv4_enabled    = true
      private_network = var.private_network
    }

    database_flags {
      name  = "cloudsql.iam_authentication"
      value = "on"
    }
    
    backup_configuration {
      enabled = false
    }

    user_labels = {
       instance_name = var.instance_name
    }
  }
}
