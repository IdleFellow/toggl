/*
 * This Terraform file sets up two Google Cloud Monitoring alert policies and an email notification channel 
 * to send notifications to the specified email address when the policies are triggered.
 * 
 * The first policy, named "SQL CPU Usage Alert," sets up a threshold condition that triggers when the CPU utilization 
 * of a specified Cloud SQL instance exceeds the threshold value. 
 * The condition filters for the cloudsql.googleapis.com/database/cpu/utilization metric and uses the instance name specified in the var.instance_name variable. 
 * The policy is triggered when the threshold is exceeded for 60 seconds, and a notification is sent to the email channel.
 * 
 * The second policy, named "SQL Disk Usage Alert," sets up a threshold condition that triggers when the disk utilization 
 * of a specified Cloud SQL instance exceeds the threshold value. 
 * The condition filters for the cloudsql.googleapis.com/database/disk/utilization metric and uses the instance name specified in the var.instance_name variable. 
 * The policy is triggered when the threshold is exceeded for 60 seconds, and a notification is sent to the email channel.
 * 
 * The email notification channel is set up with the email address specified in the var.alert_recipient variable.
 */

resource "google_monitoring_alert_policy" "sql_cpu_alert" {
  display_name = "SQL CPU Usage Alert"
  combiner     = "OR"

  conditions {
    display_name      = "${var.instance_name} CPU Usage"
    
    condition_threshold {
      filter          = "metric.type=\"cloudsql.googleapis.com/database/cpu/utilization\" AND resource.type=\"cloudsql_database\" AND metadata.user_labels.instance_name=\"${var.instance_name}\""
      threshold_value = var.cpu_alert_threshold
      comparison      = "COMPARISON_GT"
      duration        = "60s"
      trigger {
        count = 1
      }
    }
  }

  notification_channels = [google_monitoring_notification_channel.email_channel.name]
}

resource "google_monitoring_alert_policy" "sql_disk_usage_alert" {
  display_name = "SQL Disk Usage Alert"
  combiner     = "OR"

  conditions {
    display_name      = "${var.instance_name} Disk Usage"
    
    condition_threshold {
      filter          = "metric.type=\"cloudsql.googleapis.com/database/disk/utilization\" AND resource.type=\"cloudsql_database\" AND metadata.user_labels.instance_name=\"${var.instance_name}\""
      threshold_value = var.disk_usage_alert_threshold
      comparison      = "COMPARISON_GT"
      duration        = "60s"
      trigger {
        count = 1
      }
    }
  }

  notification_channels = [google_monitoring_notification_channel.email_channel.name]
}

resource "google_monitoring_notification_channel" "email_channel" {
  type = "email"

  labels = {
    email_address = var.alert_recipient
  }
}
