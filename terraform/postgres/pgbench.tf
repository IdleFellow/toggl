/*
 * This Terraform file defines a Google Cloud Run job resource named "pgbench". 
 * The job will use a custom Docker image specified in the "pgbench_image" variable to run a Postgres benchmarking tool called "pgbench" 
 * against a Cloud SQL instance.
 * 
 * The job is defined in the "BETA" launch stage and located in the region specified in the "region" variable. 
 * The job is using a Cloud Run template to define its properties, including the job name, location, and Docker container configuration.
 * 
 * The Cloud SQL instance is mounted as a volume in the job using the "cloudsql" volume name, 
 * and its connection details are specified by the "connection_name" attribute of the "google_sql_database_instance.primary_instance" resource. 
 * The Postgres database name, username, and password used in the benchmarking command are specified by the "db_name", "db_user", and "db_password" variables, 
 * respectively.
 * 
 * Overall, this Terraform file is used to deploy a Cloud Run job that runs a Postgres benchmarking tool against a Cloud SQL instance.
 */

resource "google_cloud_run_v2_job" "pgbench" {
    name               = var.pgbench_job
    location           = var.region
    launch_stage       = "BETA"

    template {
        template{
            volumes {
                name = "cloudsql"
                cloud_sql_instance {
                    instances = [google_sql_database_instance.primary_instance.connection_name]
                }
            }

            containers {
                image = var.pgbench_image

                command = [ 
                            "pgbench", 
                            "-h", "/cloudsql/${google_sql_database_instance.primary_instance.connection_name}", 
                            "-U", "${var.db_user}", 
                            "-i", 
                            "${var.db_name}" 
                        ]

                env {
                    name  = "PGPASSWORD"
                    value = "${var.db_password}"
                }


                volume_mounts {
                    name = "cloudsql"
                    mount_path = "/cloudsql"
                }
            }
        }
    }
}
