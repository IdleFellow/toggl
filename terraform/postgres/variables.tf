// variable "project_id" {}
variable "region" {}
variable "zone" {}

variable "instance_name" {}
variable "replica_name" {}

variable "protected" {
    default = "true"
}

variable "db_version" {}
variable "db_tier" {}
variable "db_size" {}

variable "replica_zone" {}

variable "db_user" {}
variable "db_password" {}
variable "db_name" {}

variable "private_network" {}

variable "pgbench_job" {}
variable "pgbench_image" {}

variable "alert_recipient" {}
variable "cpu_alert_threshold" {}
variable "disk_usage_alert_threshold" {}
