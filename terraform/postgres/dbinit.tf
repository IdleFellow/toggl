/*
 * This Terraform file is defining two resources for Google Cloud SQL - a database user and a database.
 * 
 * The google_sql_user resource creates a new user for the Google Cloud SQL instance specified in the instance attribute. 
 * The user's name and password are specified by the name and password attributes, 
 * which are set using the values of the db_user and db_password variables respectively. 
 * The resource also includes a depends_on block, which ensures that the database instance created by the google_sql_database_instance resource 
 * is created before the user is created.
 * 
 * The google_sql_database resource creates a new database within the Google Cloud SQL instance specified in the instance attribute. 
 * The database's name is specified by the name attribute, which is set using the value of the db_name variable. 
 * The resource also includes a depends_on block, which ensures that the database user created by the google_sql_user resource 
 * is created before the database is created.
 */

resource "google_sql_user" "master" {
  depends_on = [
    google_sql_database_instance.primary_instance
  ]
  name     = var.db_user
  instance = google_sql_database_instance.primary_instance.name
  password = var.db_password
}

resource "google_sql_database" "main" {
  depends_on = [
    google_sql_user.master
  ]
  name     = var.db_name
  instance = google_sql_database_instance.primary_instance.name
}
