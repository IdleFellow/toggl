/*
 * This Terraform file sets up a highly available PostgreSQL infrastructure on GCP with backup functionality.
 *
 * This Terraform file defines a GCP infrastructure deployment that includes:
 * - a Cloud SQL PostgreSQL instance with replication
 * - a private network
 * - a Cloud Storage bucket for backups.
 *
 * The provider block sets up the Google Cloud provider with the project, region, and zone variables defined in the var block.
 *
 * The module "postgres" block sets up a Cloud SQL PostgreSQL instance and replica using the source defined in the ./postgres directory. 
 * It specifies several parameters, including:
 * - the instance and replica names
 * - database credentials
 * - database version and size
 * - private network
 * - PGbench job configuration. 
 * It also sets up alert thresholds for CPU usage and disk usage.
 * 
 * The module "network" block sets up a private network using the source defined in the ./network directory. It specifies the network and IP names.
 * 
 * The module "sql-db_backup" block sets up a backup module for the PostgreSQL instance 
 * using the source from the official Google Cloud Terraform module for SQL backups. 
 * It specifies the backup retention time, backup schedule, and the GCS export URI.
 *
 * Finally, the resource "google_storage_bucket" "backup_bucket" block sets up a Cloud Storage bucket for backups. 
 * It specifies the bucket name, location, storage class, and lifecycle rule to delete backups older than 15 days.
 * 
 */

provider "google" {
  project = var.project
  region  = var.region
  zone    = var.zone
}

module "postgres" {
  source                      = "./postgres"
  region                      = var.region
  zone                        = var.zone
       
  instance_name               = var.instance_name
  replica_name                = var.replica_name
         
  protected                   = "false"
         
  db_version                  = "POSTGRES_12"
  db_tier                     = "db-f1-micro"
  db_size                     = "100"
         
  replica_zone                = var.replica_zone
         
  db_user                     = "postgres"
  db_password                 = var.db_password
  db_name                     = "main"
         
  private_network             = module.network.private_self_link
         
  pgbench_job                 = "pgbench-job"
  pgbench_image               = "postgres:12"
       
  alert_recipient             = "dba@example.com"
  cpu_alert_threshold         = ".9"
  disk_usage_alert_threshold  = ".85"
}

module "network" {
  source             = "./network"

  network_name       = var.network_name
  ip_name            = var.ip_name
}

module "sql-db_backup" {
  source                = "GoogleCloudPlatform/sql-db/google//modules/backup"
  version               = "14.0.1"

  region                = var.region
  project_id            = var.project

  sql_instance          = var.replica_name

  backup_retention_time = 15
  backup_schedule       = "59 23 * * *"
  
  export_uri            = "gs://com-toggl-derinaldis-daily-backups"
}

resource "google_storage_bucket" "backup_bucket" {
    name          = "com-toggl-derinaldis-daily-backups"
    location      = "EU"
    storage_class = "STANDARD"

    lifecycle_rule {
        action {
            type = "Delete"
        }

        condition {
            age = "15"
        }
    }
}
