// the GCP project id is read from the environment variable TF_VAR_project
variable "project" {
    default = "override me"
}
variable "region" {
    default = "europe-west1"
}
variable "zone" {
    default = "europe-west1-b"
}

variable "instance_name" {
    default = "toggl-postgres-instance"
}

variable "replica_name" {
    default = "toggl-postgres-replica"
}

// the database password is read from the environment variable TF_VAR_db_password
variable "db_password" {}

variable "replica_zone" {
    default = "europe-west1-c"
}

variable "network_name" {
    default = "private-network-toggl"
}
variable "ip_name" {
    default = "private-ip-toggl"
}
