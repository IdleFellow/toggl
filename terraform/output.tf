output "Run_this_command_to_initialise_the_database" {
  value = "gcloud beta run jobs execute ${module.postgres.pgbench_job} --wait --region ${var.region}"
}