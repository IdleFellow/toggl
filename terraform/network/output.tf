output "private_self_link" {
  value       = google_compute_network.private_network.self_link
  description = "The private network self link."
}
