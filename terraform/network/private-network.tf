/*
 * This Terraform code provisions a private network, a global internal IP address, and a private VPC connection to the service network 
 * using Google Cloud Platform (GCP) resources.
 * The google_compute_network resource creates a private network with the specified name, 
 * while the google_compute_global_address resource provisions an internal IP address for the network. 
 * The IP address is reserved for VPC peering purposes, has a prefix length of 16, and is associated with the private network via its network attribute.
 *
 * Finally, the google_service_networking_connection resource establishes a private VPC connection to the service network. 
 * It specifies the service network's endpoint as servicenetworking.googleapis.com and the reserved peering ranges for the connection, 
 * which is the internal IP address that we created earlier.
 */

resource "google_compute_network" "private_network" {
  provider = google
  name     = var.network_name
}

resource "google_compute_global_address" "private_ip_address" {
  provider      = google
  name          = var.ip_name
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = google_compute_network.private_network.self_link
}

resource "google_service_networking_connection" "private_vpc_connection" {
  provider                = google
  network                 = google_compute_network.private_network.self_link
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]
}
