#!/usr/bin/env bash

# The script is a simple and effective way to run Terraform inside a Docker container with the necessary environment variables and command-line options.

# This is a Bash script that is designed to run Terraform inside a Docker container, for provisioning a Google Cloud Platform (GCP) infrastructure. 
# The script takes several command-line options for configuration.

# First, it sets a few variables that can be overridden via environment variables or command-line options: 
# - NOACT
# - AUTO_APPROVE
# - GCP_CREDENTIALS
# - PROJECT_ID
# - DB_PASSWORD.

# Then, it defines a usage message and sets up a while loop to parse command-line options using getopts. The options are:
#         -h: Displays the usage message and exits.
#         -n: Sets the NOACT variable to 1.
#         -a: Sets the AUTO_APPROVE variable to 1.
#         -c: Specifies the path to the GCP credentials file or key.
#         -p: Specifies the GCP project ID.
#         -d: Specifies the database password.

# After parsing the options, the script checks that the required variables are set (GCP_CREDENTIALS, PROJECT_ID, and DB_PASSWORD). 
# If any of them are missing, it prints an error message and exits.

# Next, the script sets the INTERACTIVE variable based on whether or not NOACT and AUTO_APPROVE are set. 
# If neither is set, it sets INTERACTIVE to "-it", which will start a container in interactive mode.

# The script then checks if the GCP credentials file is a file that exists. 
# If it does, it sets GCP_CREDENTIALS to a Docker volume mount string that maps the credentials file to /gcp-key.json inside the container. 
# If the file doesn't exist, it sets GCP_CREDENTIALS to an environment variable that points to the file path.

# The script changes directory to the location of the script and builds a Docker image called toggl using the Dockerfile in the current directory.

# Finally, the script starts a Docker container using the toggl image, 
# mounting the terraform directory to /terraform inside the container, 
# passing in the necessary environment variables (PROJECT_ID, DB_PASSWORD, NOACT, and AUTO_APPROVE)
# and mapping the GCP credentials file to /gcp-key.json inside the container.

NOACT=${NOACT:-}
AUTO_APPROVE=${AUTO_APPROVE:-}
GCP_CREDENTIALS=${GCP_CREDENTIALS:-}
PROJECT_ID=${PROJECT_ID:-}
DB_PASSWORD=${DB_PASSWORD:-}


USAGE="provision.sh [options]
options:
 -h                 ) displays this message
 -n                 ) do not apply the configuration (NOACT)
 -a                 ) auto approve configuration changes (AUTO_APPROVE)
 -c gcp_credentials ) GCP credentials file or key (GCP_CREDENTIALS)
 -p project_id      ) GCP project id (PROJECT_ID)
 -d db_pasword      ) database password (DB_PASSWORD)"

while getopts "hnac:p:d:" OPT; do
  case "$OPT" in
    h) # help
      echo "$USAGE"
      exit 0
      ;;
    n) # no act (NOACT)
      NOACT=1
      ;;
    a) # auto approve configuration changes (AUTO_APPROVE)
      AUTO_APPROVE=1
      ;;
    c) # GCP credentials file or key (GCP_CREDENTIALS)
      GCP_CREDENTIALS="$OPTARG"
      ;;
    p) # GCP project id (PROJECT_ID)
      PROJECT_ID="$OPTARG"
      ;;
    d) # database password (DB_PASSWORD)"
      DB_PASSWORD="$OPTARG"
      ;;
  esac
done

shift "$((OPTIND-1))"

if [ -z "$GCP_CREDENTIALS" ]; then
  echo "GCP_CREDENTIALS not set" 2>&1
  exit 1
fi

if [ -z "$PROJECT_ID" ]; then
  echo "PROJECT_ID not set" 2>&1
  exit 1
fi

if [ -z "$DB_PASSWORD" ]; then
  echo "DB_PASSWORD not set" 2>&1
  exit 1
fi

INTERACTIVE=
if [ -z "$NOACT" -a -z "$AUTO_APPROVE" ]; then
  INTERACTIVE="-it"
fi

if [ -f "$GCP_CREDENTIALS" ]; then
  GCP_DIR=$(dirname "$GCP_CREDENTIALS")
  GCP_DIR=$(cd "$GCP_DIR"; pwd)
  GCP_CREDENTIALS="-v $GCP_DIR"/$(basename "$GCP_CREDENTIALS")":/gcp-key.json"
else
  GCP_CREDENTIALS="-e GOOGLE_APPLICATION_CREDENTIALS=$GCP_CREDENTIALS"
fi

cd "$(dirname "$0")"

docker build -t toggl docker

docker run \
  $INTERACTIVE \
  --rm \
  --name toggl \
  -v "$(pwd)/terraform":/terraform \
  $GCP_CREDENTIALS \
  -e TF_VAR_project=$PROJECT_ID \
  -e TF_VAR_db_password=$DB_PASSWORD \
  -e NOACT=$NOACT \
  -e AUTO_APPROVE=$AUTO_APPROVE \
  toggl
